'use strict'

const products = [

	{
		id: 1,
		name: 'Устрицы по рокфеллеровски', 
		imgSrc: 'img/main_menu/1.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '2 700',
		weight: 500
	
	},

	{
		id: 2,
		name: 'Свиные ребрышки на гриле с зеленью', 
		imgSrc: 'img/main_menu/2.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '1 600',
		weight: 750
	
	},

	{
		id: 3,
		name: 'Креветки по-королевски в лимонном соке', 
		imgSrc: 'img/main_menu/3.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '1 600',
		weight: 750
	
	},

	{
		id: 4,
		name: 'Устрицы по рокфеллеровски', 
		imgSrc: 'img/main_menu/1.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '2 700',
		weight: 500
	
	},

	{
		id: 5,
		name: 'Устрицы по рокфеллеровски', 
		imgSrc: 'img/main_menu/1.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '2 700',
		weight: 500
	
	},

	{
		id: 6,
		name: 'Свиные ребрышки на гриле с зеленью', 
		imgSrc: 'img/main_menu/2.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '1 600',
		weight: 750
	
	},

	{
		id: 7,
		name: 'Креветки по-королевски в лимонном соке', 
		imgSrc: 'img/main_menu/3.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '1 600',
		weight: 750
	
	},

	{
		id: 8,
		name: 'Устрицы по рокфеллеровски', 
		imgSrc: 'img/main_menu/1.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '2 700',
		weight: 500
	
	},

	{
		id: 9,
		name: 'Свиные ребрышки на гриле с зеленью', 
		imgSrc: 'img/main_menu/2.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '1 600',
		weight: 750
	
	},

	{
		id: 10,
		name: 'Креветки по-королевски в лимонном соке', 
		imgSrc: 'img/main_menu/3.png',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
		price: '1 600',
		weight: 750
	
	},

];

const productsById = {};
for (var i = 0; i < products.length; i++) {
  productsById[products[i].id] = products[i];
}

let section = document.getElementById('container');
// let divCart = document.querySelector('divCart');

	products.forEach(product => {
		
	   let divCart = document.createElement('div');
	   let img = document.createElement('img');
	   img.src = product.imgSrc;
	   let divDescr = document.createElement('div');
	   let h5 = document.createElement('h5');
	   let p = document.createElement('p');
	   let divPrice = document.createElement('div');
	   let spanPrice = document.createElement('span');
	   let spanWeight = document.createElement('span');
	   let button = document.createElement('button');
	   let buttonImg = document.createElement('img')


	   let sumQty = document.getElementById('sum__qty');
	   let sumTotal = document.getElementById('sum__total');

	   button.setAttribute('data-price', product.price); 
	   button.setAttribute('data-id', product.id);

	   h5.textContent = product.name;
	   p.textContent = product.description;
	   spanPrice.textContent = product.price + ' р. ' + '/ ' + product.weight + ' гр.';
	   spanWeight.textContent = product.weight;
	   
	   divCart.classList.add('cart')
	   img.classList.add('container__good-img')
	   p.classList.add('cart__descr')
	   divPrice.classList.add('cart__add-in-basket')
	   button.classList.add('cart__add-in-basket-button')
	   buttonImg.classList.add('plus')
	   // document.querySelector("plus").src="/plus";
	   buttonImg.src = "img/plus.png"


	divCart.appendChild(img);
	divCart.appendChild(divDescr);
	divDescr.appendChild(h5);
	divDescr.appendChild(p);
	divDescr.appendChild(divPrice);
	divPrice.appendChild(spanPrice);
	// divPrice.appendChild(spanWeight);
	divPrice.appendChild(button);
	button.appendChild(buttonImg);



	// container.appendChild(divCart);

	// container.appendChild(divCart);
	// container.appendChild(img);
	// container.appendChild(divDescr);
	// container.appendChild(h5);
	// container.appendChild(p);
	// container.appendChild(divPrice);
	// container.appendChild(spanPrice);
	// container.appendChild(spanWeight);
	// container.appendChild(button);



	section.appendChild(divCart);

	}) 

 const buttonColection = document.getElementsByTagName('button');

 for (let htmlButton of buttonColection) {
 	htmlButton.addEventListener('click', (e) => {
		let id = e.target.getAttribute('data-id')
        let price = e.target.getAttribute('data-price')

		if (localStorage.getItem('cart-items') == null) {
			localStorage.setItem('cart-items', '[]');
		}


		let itemExists = false;
		let cartItems = JSON.parse(localStorage.getItem('cart-items'));
		cartItems.forEach(item => {
			if (item.id === id) {
				itemExists = true;

				item.qty++;
			}
		})


		if (!itemExists) {
			let newItem = {
				id: id,
				qty: 1
			};

			cartItems.push(newItem);
		}

		localStorage.setItem('cart-items', JSON.stringify(cartItems));

		// refreshCartInHeader()
	})


 	//  function refreshCartInHeader() {
 	//  	let sum = 0;
 	//   	let cartItems = JSON.parse(localStorage.getItem('cart-items'));
		// cartItems.forEach(item => {
		// 	if (item.id === products[i].id) {
		// 		sum += products[i].price;
		// 	}
			
 	//  })


 }
 
 	


