"use strict"

	const UserEmail = 'd.can@mail.ru'
	const UserPassword = '12345678'

function validateEmail(email) {
	console.log(email)
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}



let email = document.getElementById('email-input');

let password = document.getElementById('form__password-input');
let checkbox = document.getElementById('input-checkbox');
let btn = document.getElementById('btn');
let error_email = document.querySelector('.error_email')
let error_email_valid = document.querySelector('.error_email_valid')
let error_password = document.querySelector('.error_password')
let error_password_valid = document.querySelector('.error_password_valid')
let form_email = document.getElementById('form_email')
let form_password = document.getElementById('form_password')
let agree_chbox = document.getElementById('input-checkbox')
let checkboxMark = document.querySelector('.form__checkbox-mark')
let error_check = document.querySelector('.error_check')
let star_check = document.getElementById('star_check')

console.log({form_email})
console.log(agree_chbox.checked)


function onchang() {
	if (agree_chbox.checked) {
		error_check.style.display = 'none';
 		checkboxMark.style = 'border: 1px solid  #787878'
	}
	else {
		error_check.style.display = 'block';
		checkboxMark.style = 'border: 1px solid red'
	}
}



email.addEventListener('input', (e) => {
	if (validateEmail(email.value)) {
		error_email.style.display = 'none';
		error_email_valid.style.display = 'none';
		form_email.classList.remove('red');
 		email.classList.remove('border_red')
	} else {
	    error_email.style.display = 'none';
 		error_email_valid.style.display = 'block';
 		form_email.classList.add('red');
 		email.classList.add('border_red')	
	}
})

password.addEventListener('input', (e) => {
	if (e.target.value.length >= 8) {
		error_password.style.display = 'none';
		error_password_valid.style.display = 'none';
		form_password.classList.remove('red');
 		password.classList.remove('border_red')
	} else {
		error_password_valid.style.display = 'block';
 		form_password.classList.add('red');
 		password.classList.add('border_red')
	}
})

 btn.addEventListener('click', (e) => {
 	e.preventDefault();


 	if (email.value == '') {
 		error_email_valid.style.display = 'none';
 		error_email.style.display = 'block';
 		form_email.classList.add('red');
 		email.classList.add('border_red')
 		// return;
 	} else {
 		error_email.style.display = 'none';
 	}


 	if (email.value != '' && !validateEmail(email.value)) {
 		error_email.style.display = 'none';
 		error_email_valid.style.display = 'block';
 		form_email.classList.add('red');
 		email.classList.add('border_red')
 		// return;
 	} else {
 		error_email_valid.style.display = 'none';
 	}


 	if (password.value == '') {
 		console.log(password.value)
 		error_password_valid.style.display = 'none';
 		error_password.style.display = 'block';
 		form_password.classList.add('red');
 		password.classList.add('border_red')
 		//return;
 	} else {
 		error_password.style.display = 'none';
 	}


 	if (password.value != '' && password.value.length < 8) {
 		error_password_valid.style.display = 'block';
 		form_password.classList.add('red');
 		password.classList.add('border_red')
 		
 	} else {
 		error_password_valid.style.display = 'none';
 	}


 	onchang();


	if (email.value === UserEmail && password.value === UserPassword) {
		const catalogPage = (location.href).substr(0, location.href.lastIndexOf('/') + 1) + 'catalog.html';
 		window.location.href = catalogPage;
 	}
 	else {
 		alert('Пользователь не найден')
 	}
})
